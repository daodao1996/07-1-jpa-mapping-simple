package com.twuc.webApp.domain.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private EntityManager em;

    @Test
    void should_pass() {
        assertTrue(true);
    }

//    @Test
//    void should_get_a_person() {
//        Optional<Person> person = personRepository.findById(1L);
//        assertThat(person.isPresent()).isTrue();
//    }

    @Test
    void should_save_a_person() {
        Person person = new Person(1L,"yidao","zhao");
        personRepository.saveAndFlush(person);
//        personRepository.flush();
        em.clear();
        Optional<Person> findPerson = personRepository.findById(1L);
        assertThat(findPerson.isPresent()).isTrue();
        assertThat(findPerson.get().getFirstName()).isEqualTo("yidao");
    }
}
